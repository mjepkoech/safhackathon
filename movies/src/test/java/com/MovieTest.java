package com;

import com.movies.demo.models.Movie;
import com.movies.demo.models.MovieRepo;
import com.movies.demo.services.MovieOperations;
import org.jboss.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class MovieTest {
    @InjectMocks
    MovieOperations movieOperations;

    Logger logger= Logger.getLogger(this.getClass());

    @Mock
    private MovieRepo movieRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllMoviesTest()
    {
        List<Movie> list = new ArrayList<Movie>();
        Movie movOne = new Movie("my movie", "my new movie", 4, 1);
        Movie movTwo = new Movie("Annother", "Another new movie", 3, 0);

        list.add(movOne);
        list.add(movTwo);

        when(movieRepository.findAll()).thenReturn(list);
        //test
        List<Movie> movList = movieRepository.findAll();
        logger.info(movList);
    }

}
