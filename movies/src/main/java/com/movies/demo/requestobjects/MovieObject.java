package com.movies.demo.requestobjects;

public class MovieObject {
    private Integer id;
    String name;
    Integer flag;
    Integer rating;
    String description;

    public MovieObject() {
    }

    public MovieObject(Integer id, String name, Integer flag, Integer rating, String description) {
        this.id = id;
        this.name = name;
        this.flag = flag;
        this.rating = rating;
        this.description = description;
    }

    @Override
    public String toString() {
        return "MovieObject{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", flag=" + flag +
                ", rating=" + rating +
                ", description='" + description + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
