package com.movies.demo.controllers;

import com.movies.demo.models.Movie;
import com.movies.demo.models.MovieRepo;
import com.movies.demo.requestobjects.MovieObject;
import com.movies.demo.requestobjects.ResponseObj;
import com.movies.demo.services.MovieOperations;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class HomeController {
    @Autowired
    private MovieRepo movieRepository;

    @Autowired
    MovieOperations movieOperations;
    Logger logger= Logger.getLogger(this.getClass());

    @GetMapping("/")
    public List<Movie> getMovies() {
        return movieRepository.findAll();
    }

    @PostMapping("/add")
    public ResponseObj newMovie (@RequestBody MovieObject movieObject){
        return   movieOperations.createMovie(movieObject.getName(), movieObject.getDescription(),
                movieObject.getFlag(), movieObject.getRating());}

    @PutMapping("/update")
    public Movie updateMovie(@RequestBody Movie movie) {
        return movieRepository.save(movie);
    }

    @PostMapping("/watched")
    public ResponseObj getMovie (@RequestBody MovieObject movieObject){
      return   movieOperations.checkMovie(movieObject.getFlag());
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteMovie(@PathVariable Integer id) {
        movieRepository.deleteById(id);
        return true;
    }


}
