package com.movies.demo.models;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovieRepo extends JpaRepository<Movie, Integer> {
    int countByName(String name);
    List<Movie> findByFlag(Integer flag);
    List<Movie> findAll();
}
