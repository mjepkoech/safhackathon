package com.movies.demo.services;

        import com.movies.demo.models.Movie;
        import com.movies.demo.models.MovieRepo;
        import com.movies.demo.requestobjects.ResponseObj;
        import org.jboss.logging.Logger;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;

        import java.util.List;

@Service
public class MovieOperations {
    Logger logger= Logger.getLogger(this.getClass());

    @Autowired
    MovieRepo movieRepo;

    public ResponseObj createMovie (String name, String description, Integer flag, Integer rating){
        int existing = movieRepo.countByName(name);

        if(existing==0) {
            Movie newmovie = new Movie();
            newmovie.setName(name);
            newmovie.setDescription(description);
            newmovie.setFlag(flag);
            newmovie.setRating(rating);

            movieRepo.save(newmovie);
            return new ResponseObj("1", "Successfully created a new movie.");
        }else {
            return new ResponseObj("0", "Error occurred while creating movie");
        }
    }

    public ResponseObj checkMovie(Integer flag) {
        List<Movie> existing = movieRepo.findByFlag(flag);

        logger.info(flag);
        if(existing.isEmpty()) {
            return new ResponseObj("0", "No movie found");
        }
        else {
            return new ResponseObj("1", existing.toString());
        }
    }

}
