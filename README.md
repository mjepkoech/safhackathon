# SafHackathon

## Microservices with Spring Boot for a movies system
The source code for Safaricom Hackathon on Microservices with Spring Boot. 

This is a simple web service that registers users and can do a
number of CRUD operations like add movies, edit, fetch and delete.

The web service is based on REST API design principles and utilize the above technologies listed under requirements.

### Requirements 
- Java 1.8
- Spring Boot (2+)
- A relational database (MySQL in this case)
- Maven(3.5+)

## How to run?

### Build all modules:

`mvn clean package -DskipTests=true`

### Start each microservice either in local command line or IDE:

**Local command line:** `safhackathon/user> mvn spring-boot:run`

**IDE:** Build and run the microservices directly from your IDE. Start with the server and gateway before starting the rest.

NB: Docker files are also included if one may want to use docker for containerization

* MySQL container:
     * hostname: milly
     * Port : 3306
     * Username/Password: root/Mine12345

* Server Microservice:
    * hostname: eureka-server
    * Port: 8761
    * URL: http://localhost:8761/

* Gateway Microservice:
    * hostname: zuul-gateway
    * Ports: 8762
    * URL: http://localhost:8762/
    
* User Microservice:
    * hostname: user-service
    * Port: 6090
    * URL: http://localhost:6090/register
    * Gateway URL: http://localhost:8762/register

* Auth Microservice:
    * hostname: auth-service
    * Port: 9100
    * URL: http://localhost:9100/auth
    * Gateway URL: http://localhost:8762/auth
    
* Movies Microservice   
    * hostname: movie-service
    * Port: 6091
    * URL: http://localhost:6091/movie
    * Gateway URL: http://localhost:8762/movie
