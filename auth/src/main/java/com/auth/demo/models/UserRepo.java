package com.auth.demo.models;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepo extends JpaRepository<UserMode, Long> {
    List<UserMode> findByUsername(String username);
}
