//package com.user.demo.services;
//
//import com.user.demo.models.User;
//import com.user.demo.models.UserRepo;
//import com.user.demo.requestobjects.ResponseObj;
//import org.jboss.logging.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class UserOperations {
//    Logger logger= Logger.getLogger(this.getClass());
//
//    @Autowired
//    UserRepo userRepo;
//
//    public ResponseObj createUser (String username, String password, String email, Long phonenumber){
//        int existing = userRepo.countByEmailOrPhonenumber(email,phonenumber);
//
//        if(existing==0) {
//            User newuser = new User();
//            newuser.setUsername(username);
//            newuser.setPassword(password);
//            newuser.setPhonenumber(phonenumber);
//            newuser.setEmail(email);
//
//            userRepo.save(newuser);
//            return new ResponseObj("1", "Successfully created a new user.");
//        }else {
//            return new ResponseObj("0", "Error occured while creating user");
//        }
//    }
//
//    public ResponseObj checkUser(String username, String password, String email, Long phonenumber) {
//        List<User> existing = userRepo.findByUsernameAndPassword(username,password);
//
//        logger.info(username + password);
//        if(existing.isEmpty()) {
//            return new ResponseObj("0", "The user does not exist");
//        }
//        else {
//            User newuser = new User();
//            newuser.setUsername(username);
//            newuser.setPassword(password);
//            newuser.setPhonenumber(phonenumber);
//            newuser.setEmail(email);
//
//            userRepo.save(newuser);
//            User user=existing.get(0);
//
//            return new ResponseObj("1", "Successfully created a new user.");
//        }
//    }
//
//}


package com.user.demo.services;

        import com.user.demo.models.User;
        import com.user.demo.models.UserRepo;
        import com.user.demo.requestobjects.ResponseObj;
        import org.jboss.logging.Logger;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.context.annotation.Bean;
        import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
        import org.springframework.stereotype.Service;

        import java.util.List;

@Service
public class UserOperations {
    Logger logger= Logger.getLogger(this.getClass());

    @Autowired
    UserRepo userRepo;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public ResponseObj createUser (String username, String password, String email, Integer phonenumber, String role){
        int existing = userRepo.countByEmailOrPhonenumber(email,phonenumber);

        if(existing==0) {
            User newuser = new User();
            newuser.setUsername(username);
            newuser.setPassword(encoder.encode(password));
            newuser.setPhonenumber(phonenumber);
            newuser.setEmail(email);
            newuser.setRole(role);

            userRepo.save(newuser);
            return new ResponseObj("1", "Successfully created a new user.");
        }else {
            return new ResponseObj("0", "Error occurred while creating user");
        }
    }

    public ResponseObj checkUser(String username, String password) {
        List<User> existing = userRepo.findByUsernameAndPassword(username,password);

        logger.info(username + password);
        if(existing.isEmpty()) {
            return new ResponseObj("0", "Incorrect credentials");
        }
        else {
            User user=existing.get(0);

            return new ResponseObj("1",user.toString());
        }
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
