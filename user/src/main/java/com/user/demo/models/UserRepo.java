package com.user.demo.models;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepo extends JpaRepository<User, Integer> {
    int countByEmailOrPhonenumber(String email,Integer phonenumber);
    List<User> findByUsernameAndPassword(String username, String password);
}
