package com.user.demo.controllers;

        import com.user.demo.requestobjects.ResponseObj;
        import com.user.demo.requestobjects.UserObject;
        import com.user.demo.services.UserOperations;
        import org.jboss.logging.Logger;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.*;


@RestController
public class HomeController {

    @Autowired
    UserOperations userOperations;
    Logger logger= Logger.getLogger(this.getClass());

    @PostMapping("/")
    public ResponseObj newUser (@RequestBody UserObject userObject){
        return   userOperations.createUser(userObject.getUsername(),userObject.getPassword(),
                userObject.getEmail(),userObject.getPhonenumber(), userObject.getRole());
    }

}
