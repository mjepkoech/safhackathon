package com.user.demo.requestobjects;

public class UserObject {
    String username;
    Integer phonenumber;
    String password;
    String email;
    String role;

    public UserObject() {
    }

    public UserObject(String username, Integer phonenumber, String password, String email, String role) {
        this.username = username;
        this.phonenumber = phonenumber;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserObject{" +
                "username='" + username + '\'' +
                ", phonenumber=" + phonenumber +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(Integer phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
